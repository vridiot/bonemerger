# BoneMerger

A simple blender addon to merge bones to their parents, along with their weights

## Installation
Download the .py file, open preferences in blender, click Install and select the .py file.
Enable it (it can be found in the Rigging category)

## Usage
Select a bone, click the Pose menu at the bottom of the 3D view, click the Bone Merger menu item.
The bone will merge with its parent and that bones weights will be mixed into the parent's weights using the max() function.

## Limitations
Currently only does one bone at a time, that will change in a future release

## Bugs
Hopefully none, but do file an issue if you find any.

## Licence
GPL v2
