bl_info = {
    "name": "Bone Merger",
    "author": "John W",
    "version": (1, 0),
    "blender": (3, 0, 0),
    "location": "Pose Menu > Bone Merger",
    "description": "Merges a single bone into its parent along with its weights using max()",
    "warning": "",
    "doc_url": "",
    "category": "Rigging",
}


import bpy

def main(context):
    ar = str(bpy.context.active_pose_bone.id_data.name)

    bn1=bpy.context.active_pose_bone.name
    bnp=bpy.context.active_pose_bone.parent.name

    print("Armature is: "+ar)


    y = ""
    if bpy.data.objects[ar].children:
        for c in bpy.data.objects[ar].children:
            if c.modifiers['Armature']:
                print("Child has armature modifier, probably the object we need")
                y = c.name
    else:
        for x in bpy.data.objects:
            try:
                if x.modifiers['Armature'].object.name == ar:
                    y = x.name
            except:
                pass

    print("Object is: "+str(y))

    bpy.data.objects[y].modifiers.new('VertexWeightMix','VERTEX_WEIGHT_MIX')

    bpy.data.objects[y].modifiers['VertexWeightMix'].vertex_group_a = bn1
    bpy.data.objects[y].modifiers['VertexWeightMix'].vertex_group_b = bnp
    bpy.data.objects[y].modifiers["VertexWeightMix"].mix_mode = 'MAX'

    tcontext = bpy.context.copy()
    tcontext['object'] = bpy.data.objects[y]

    with bpy.context.temp_override(**tcontext):
        bpy.ops.object.modifier_move_to_index(modifier='VertexWeightMix', index=0)
        bpy.ops.object.modifier_apply(modifier='VertexWeightMix')

    prev_mode = bpy.context.selected_objects[0].mode
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.armature.dissolve()
    bpy.ops.object.mode_set(mode=prev_mode)


class BoneMerger(bpy.types.Operator):
    """Merges selected bone into parent"""
    bl_idname = "armature.bone_merger"
    bl_label = "Bone merger"
    
    def execute(self, context):
        main(context)
        return {'FINISHED'}
    
def menu_func(self, context):
    self.layout.operator(BoneMerger.bl_idname, text=BoneMerger.bl_label)

def register():
    bpy.utils.register_class(BoneMerger)
    bpy.types.VIEW3D_MT_pose.append(menu_func)

def unregister():
    bpy.utils.unregister_class(BoneMerger)
    bpy.types.VIEW3D_MT_pose.remove(menu_func)

if __name__ == "__main__":
    register()